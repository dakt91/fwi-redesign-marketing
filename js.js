var typed = new Typed(".typed", {
    strings: ["sleeker", "faster", "new"],
    smartBackspace: true, // Default value
    typeSpeed: 100,	// typing speed
    startDelay: 700,
    backSpeed: 60,
    backDelay: 700,
    loop: false,
  });
